import greenfoot.*;
import java.util.*;


class Consumer extends Actor {
	public static final int SIZE = ProducerConsumerWorld.CELL * 3 / 2;
	public static final String TITLE = "KONSUMENT";

	private final Random rng;
	private final long delayLower;  // lower delay bound between crates in ms
	private final long delayUpper;  // upper delay bound between crates in ms
	private long delay;  // time between the last take and the next
	private long taken;  // time the last crate was taken
	private Crate current;
	private Storage storage;

	public Consumer(long delayLower, long delayUpper, Storage storage) {
		super();

		this.rng = new Random();
		this.storage = storage;
		this.delayUpper = delayUpper;
		this.delayLower = delayLower;

		GreenfootImage image = new GreenfootImage("images/consumer.png");
		GreenfootImage text = new GreenfootImage(
				TITLE,
				SIZE / 6,
				Color.WHITE,
				new Color(0, 0, 0, 0),
				Color.BLACK);

		image.scale(SIZE, SIZE);
		image.drawImage(text, (SIZE - text.getWidth()) / 2, (SIZE / 4 - text.getHeight()) / 2);

		this.setImage(image);
	}

	public void takeCrate() {
		this.taken = System.currentTimeMillis();
		this.current = this.storage.take();

		if (this.current != null)
			this.current.setLocation(this.getX(), this.getY());

		this.delay = this.delayLower + (long)this.rng.nextInt((int)(this.delayUpper - this.delayLower));
	}

	public void act() {
		float animation = (System.currentTimeMillis() - this.taken) / (float)this.delay;

		if (animation > 1.0) {
			if (this.current != null) {
				this.current.setTransparency(0);
				this.getWorld().removeObject(this.current);
			}
			this.takeCrate();
		}
		else {
			if (this.current != null)
				this.current.setTransparency((int)(255 * (1 - animation)));
		}
	}
}

