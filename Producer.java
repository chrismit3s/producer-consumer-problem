import greenfoot.*;


class Producer extends Actor {
	public static final int SIZE = ProducerConsumerWorld.CELL * 3 / 2;
	public static final String TITLE = "ERZEUGER";

	private final long delay;  // delay between crates in ms
	private long created;  // time the last crate was created in ms
	private Crate next;
	private int i;
	private String prefix;
	private Storage storage;

	public Producer(long delay, Storage storage) {
		this(delay, "", storage);
	}

	public Producer(long delay, String prefix, Storage storage) {
		super();

		this.storage = storage;
		this.delay = delay;
		this.i = 0;
		this.prefix = prefix;

		GreenfootImage image = new GreenfootImage("images/producer.png");
		GreenfootImage text = new GreenfootImage(
				TITLE,
				SIZE / 6,
				Color.WHITE,
				new Color(0, 0, 0, 0),
				Color.BLACK);

		image.scale(SIZE, SIZE);
		image.drawImage(text, (SIZE - text.getWidth()) / 2, (SIZE / 4 - text.getHeight()) / 2);

		this.setImage(image);
	}

	public void nextCrate() {
		this.created = System.currentTimeMillis();
		this.next = new Crate(this.prefix + Integer.toString(this.i++), 0);  // zero so its transparent
		this.getWorld().addObject(this.next, this.getX(), this.getY());
	}

	public void act() {
		float animation = (System.currentTimeMillis() - this.created) / (float)this.delay;
		
		if (animation > 1.0) {  // animation done
			if (this.next != null)
				this.next.setTransparency(255);

			// if the storage accepts it, we move it
			if (this.next == null || this.storage.put(this.next))
				this.nextCrate();
		}
		else {
			if (this.next != null)
				this.next.setTransparency((int)(255 * animation));
		}
	}
}
