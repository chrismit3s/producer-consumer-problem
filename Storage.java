import greenfoot.*;
import java.util.*;


class Storage extends Actor {
	public static final int SIZE = ProducerConsumerWorld.CELL * 3 / 2;
	public static final String TITLE = "SPEICHER";

	private final Stack<Crate> crates;
	private final int slots;

	public Storage(int slots) {
		super();

		this.slots = slots;
		this.crates = new Stack<>();

		// we want the image to only extend below this actors actual position, so we just add some transparent pixels above
		int x = ProducerConsumerWorld.CELL * (this.slots - 1);  // amount of space above and below the at cell this actors actual position
		GreenfootImage transparent = new GreenfootImage(SIZE, SIZE + 2 * x);

		GreenfootImage imageTop = new GreenfootImage("images/storage-top.png");  // top part of the box
		GreenfootImage imageMid = new GreenfootImage("images/storage-mid.png");  // mid part of the box (will be stretched to fit the size)
		GreenfootImage imageBottom = new GreenfootImage("images/storage-bottom.png");  // bottom part of the box
		GreenfootImage image = new GreenfootImage(imageTop.getWidth(), (SIZE + x) * imageTop.getWidth() / SIZE);  // image the other parts will be painted on

		GreenfootImage text = new GreenfootImage(  // title text
				TITLE,
				SIZE / 6,
				Color.WHITE,
				new Color(0, 0, 0, 0),
				Color.BLACK);

		// build the image from top, mid and bottom
		image.drawImage(imageTop, 0, 0);
		image.drawImage(imageBottom, 0, image.getHeight() - imageBottom.getHeight());
		imageMid.scale(imageMid.getWidth(), image.getHeight() - imageBottom.getHeight() - imageTop.getHeight());
		image.drawImage(imageMid, 0, imageTop.getHeight());

		// rescale the image and paint the text
		image.scale(SIZE, SIZE + x);
		image.drawImage(text, (SIZE - text.getWidth()) / 2, (SIZE / 4 - text.getHeight()) / 2);

		// draw actual image on the bigger, transparent background
		transparent.drawImage(image, 0, x);
		this.setImage(transparent);
	}

	public boolean put(Crate crate) {
		if (this.crates.size() < this.slots) {
			this.crates.add(crate);
			crate.setLocation(this.getX(), this.getY() + this.crates.size() - 1);
			return true;
		}
		else {
			return false;
		}
	}

	public Crate take() {
		return this.crates.empty() ? null : this.crates.pop();
	}
}

