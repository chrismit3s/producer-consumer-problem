import greenfoot.*;


public class ProducerConsumerWorld extends World {
	public static final int WIDTH = 15, HEIGHT = 9;
	public static final int CELL = 120;
	public static final long DELAY = 1000;  // delay for producers and average delay for consumers in ms
	public static final long RANGE = 800;  // variability of consumtion time in ms

	public static final int NUM_PRODUCERS = 3;
	public static final int NUM_CONSUMERS = 2;
	public static final int STORAGE_SIZE = 7;

	public ProducerConsumerWorld() {
		super(WIDTH, HEIGHT, CELL);

		GreenfootImage bg = this.getBackground();
		GreenfootImage image = new GreenfootImage("images/tile.png");

		bg.scale(CELL, CELL);
		image.scale(CELL, CELL);
		bg.drawImage(bg, 0, 0);

		this.setBackground(image);

		// create storage
		Storage storage = new Storage(STORAGE_SIZE);
		this.addObject(storage, (WIDTH - 1) / 2, (HEIGHT - STORAGE_SIZE) / 2);

		// create producers
		for (int i = 0; i != NUM_PRODUCERS; ++i) {
			Producer producer = new Producer(DELAY, Character.toString('A' + i), storage);
			this.addObject(producer, 1, 1 + 2 * i);
		}

		// create consumers
		for (int i = 0; i != NUM_CONSUMERS; ++i) {
			Consumer consumer = new Consumer(DELAY - RANGE, DELAY + RANGE, storage);
			this.addObject(consumer, WIDTH - 2, 1 + 2 * i);
		}
	}
}
