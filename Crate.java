import greenfoot.*;


class Crate extends Actor {
	public static final int SIZE = 96;

	private final String title;

	public Crate(String title) {
		super();
		
		this.title = title;

		GreenfootImage image = new GreenfootImage("images/crate.png");
		GreenfootImage text = new GreenfootImage(
				this.title,
				SIZE / 2,
				Color.WHITE,
				new Color(0, 0, 0, 0),
				Color.BLACK);

		image.scale(SIZE, SIZE);
		image.drawImage(text, (SIZE - text.getWidth()) / 2, (SIZE - text.getHeight()) / 2);

		this.setImage(image);
	}

	public Crate(String title, int a) {
		this(title);
		this.setTransparency(a);
	}

	public void setTransparency(int a) {
		GreenfootImage image = this.getImage();
		image.setTransparency(a);
		this.setImage(image);
	}

	public int getTransparency() {
		return this.getImage().getTransparency();
	}
}
